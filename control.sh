#!/bin/bash
source "$(dirname $0)/script/_inc.sh"

set -e

ELASTICSEARCH_SERVICE="elasticsearch"
KIBANA_SERVICE="kibana"
BACKUP_DIR_RELATIVE_PATH="../../backups" #relative to compose dir
BACKUP_FILE_PREFIX="elasticsearch-backup"

ARG_COUNT=$#

STOP=0
START=0
STATUS=0
BACKUP=0
RESTORE=0
HELP=0
VERBOSE=0
NOPROXY=0
SECURITY=0
CURATE=0

print_usage() {
    echo ""
    echo "control.sh [start|stop|restart|backup|restore] [-h] [-d] [--security] [--no-proxy]"
    echo ""
    echo "  start       Start Elasticsearch and Kibana"
    echo "  stop        Stop Elasticsearch and Kibana"
    echo "  restart     Restart Elasticsearch and Kibana"
    echo "  status      Show status of Elasticsearch and Kibana containers"
    echo "  backup      Create a backup of the Elasticsearch index"
    echo "                This will create a new backup archive (${BACKUP_FILE_PREFIX}...tgz)"
    echo "                in the backup directory (a sibling to the script directory)"
    echo "  restore     Restore a backup of the Elasticsearch index into the current container"
    echo "                This will extract and apply the most recent backup archive"
    echo "                (${BACKUP_FILE_PREFIX}...tgz) in the backup directory  (a sibling to"
    echo "                the script directory)"
    echo "  curate      Curates the ElasticSearch indices (close old ones)"
    echo ""
    echo "  -d, --debug Run this script in verbose mode"
    echo ""
    echo "  -h, --help  Show help"
    echo ""
    echo "  --security Enable X-Pack security"
    echo ""
    echo "  --no-proxy  Disable Kibana proxy"
    echo "                Kibana will be exposed to the localhost without an nginx proxy,"
    echo "                NOT to be used in production"
}

main() {
	process_args $@
	execute_control_commands 
}

process_args() {
	#
	# Process script arguments
	#
	while [[ $# -gt 0 ]]
	do
	key="$1"
	case $key in
		'stop')
			STOP=1
			;;
		'start')
			START=1
			;;
		'restart')
			STOP=1
			START=1
			;;
		'status')
			STATUS=1
			;;
		'backup')
			BACKUP=1
			;;
		'restore')
			RESTORE=1
			;;
		'curate')
			CURATE=1
			;;
		'--security')
			SECURITY=1
		   ;;
		'-h'|'--help')
			HELP=1
		   ;;
		'-d'|'--debug')
			VERBOSE=1
			;;
		'--no-proxy')
			NOPROXY=1
			;;
		*)
			echo "Unkown option: $key"
			HELP=1
			;;
	esac
	shift # past argument or value
	done
	
	BASH_OPTS=""
	# Print parameters if running in verbose mode
	if [ ${VERBOSE} -eq 1 ]; then
		set -x
		BASH_OPTS="${BASH_OPTS} -x"
	fi
}

execute_control_commands() {
	#
	# Execute based on mode argument
	#
	if [ $ARG_COUNT -le 0 ] || [ ${HELP} -eq 1 ]; then
		print_usage
	    exit 0
	else
		COMPOSE_OPTS="-f docker-compose.yml"
		if [ ${NOPROXY} -eq 1 ]; then
			COMPOSE_OPTS="${COMPOSE_OPTS} -f no-proxy.yml"
		else
			#default: enable proxy
			COMPOSE_OPTS="${COMPOSE_OPTS} -f proxy.yml"		
		fi
		
		if [ $SECURITY -eq 1 ]; then
			COMPOSE_OPTS="${COMPOSE_OPTS} -f auth.yml"	
		fi
		
		if [ $VERBOSE -eq 1 ]; then
			COMPOSE_OPTS="${COMPOSE_OPTS} --verbose"	
		fi

		COMPOSE_OPTS="${COMPOSE_OPTS} ${EXTRA_COMPOSE_OPTS}"
		
		COMPOSE_CMD_ARGS=""
		
		BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

		COMPOSE_DIR="${BASE_DIR}/clarin"
		SCRIPT_DIR="${BASE_DIR}/script"
	
		if [ ${STATUS} -eq 1 ]; then
			ek_status
			exit 0
		fi

		if [ ${STOP} -eq 1 ]; then
			ek_stop
		fi
		if [ ${START} -eq 1 ]; then
			ek_start
		fi
		if [ ${BACKUP} -eq 1 ]; then
			ek_backup
		fi
		if [ ${RESTORE} -eq 1 ]; then
			ek_restore
		fi
		if [ ${CURATE} -eq 1 ]; then
			ek_curate
		fi
	fi
}

ek_status() {
	_docker-compose ${COMPOSE_OPTS} ps
}

ek_start() {
	if _docker-compose ${COMPOSE_OPTS} up -d ${COMPOSE_CMD_ARGS}; then
		export_credentials
		while ! curl -s -f $(curl_credentials_opt) "${ELASTIC_SEARCH_URL}" > /dev/null
		do
			echo "Waiting for Elasticsearch..."
			sleep 5
		done
	fi
}

ek_stop() {
	_docker-compose ${COMPOSE_OPTS} down ${COMPOSE_CMD_ARGS}
}

ek_backup() {
	if service_is_running ${ELASTICSEARCH_SERVICE}; then
		echo -e "Elasticsearch is running. Starting backup procedure...\n"
	else
		echo "Elasticsearch is not running. Please start Elasticsearch and try again.."
		exit 1
	fi

	export ELASTIC_COMPOSE_DIR="${COMPOSE_DIR}"

	BACKUP_DIR="${COMPOSE_DIR}/${BACKUP_DIR_RELATIVE_PATH}" #host only dir
	export ELASTIC_SEARCH_BACKUP_DIR="${BACKUP_DIR}/work-backup"

	export_credentials

	if ! (mkdir -p "${BACKUP_DIR}" && [ -d "${BACKUP_DIR}" ] && [ -x "${BACKUP_DIR}" ]); then
		echo "Cannot create and/or access backup directory ${BACKUP_DIR}"
		exit 1
	fi

	echo "Archiving old backups..."
	mkdir -p "${BACKUP_DIR}/archived"
	(
		cd "${BACKUP_DIR}"
		mv "${BACKUP_FILE_PREFIX}"*".tgz" "archived/" > /dev/null 2>&1 && echo " Done" || echo " Nothing to do"
	)

	if [ -d "${ELASTIC_SEARCH_BACKUP_DIR}" ]; then
		echo "Moving old work directory out of the way ${ELASTIC_SEARCH_BACKUP_DIR}"
		mv "${ELASTIC_SEARCH_BACKUP_DIR}" "${BACKUP_DIR}/lost+found-work-backup-$(date +%Y%m%d%H%M%S)"
	fi

	bash ${BASH_OPTS} "${SCRIPT_DIR}/backup.sh"

	if ! [ -d "${ELASTIC_SEARCH_BACKUP_DIR}" ]; then
		echo "Backup directory does not exist after backup. Failed backup?"
		exit 1
	fi

	echo "Compressing new backup..."
	COMPRESSED_BACKUP_FILE="${BACKUP_FILE_PREFIX}-$(date +%Y%m%d%H%M%S).tgz"
	(cd "${ELASTIC_SEARCH_BACKUP_DIR}" && \
		tar zcf "${COMPRESSED_BACKUP_FILE}" *)
	if mv "${ELASTIC_SEARCH_BACKUP_DIR}/${COMPRESSED_BACKUP_FILE}" "${BACKUP_DIR}/${COMPRESSED_BACKUP_FILE}"; then
		echo "Moved to ${BACKUP_DIR}/${COMPRESSED_BACKUP_FILE}. Cleaning up uncompressed backup..."
		_remove_dir "${ELASTIC_SEARCH_BACKUP_DIR}"
		echo "Done!"
	else
		echo "Creation of backup archive failed. Target directory '${ELASTIC_SEARCH_BACKUP_DIR}' left as is."
		exit 1
	fi
}

ek_restore() {
    # High level description of the restore process:
    # - check preconditions
    # - create snapshot repository using elasticsearch api
    # - mount backup tarballs under /var/restore
    # - uncompress backup tarball to /var/backup
    # - restore respository from /var/backup using the elasticsearch api
    # - cleanup
    #
	BACKUP_DIR="${COMPOSE_DIR}/${BACKUP_DIR_RELATIVE_PATH}" #host only dir
	if ! [ -d "${BACKUP_DIR}" ]; then
		echo "Backup directory ${BACKUP_DIR} not found! Place a backup file in this location and try again."
		exit 1
	fi
	
	LAST_BACKUP_FILE=`find "${BACKUP_DIR}" -maxdepth 1 -name "${BACKUP_FILE_PREFIX}*.tgz" | sort | tail -n 1`	
	if [ "${LAST_BACKUP_FILE}" = "" ] || ! [ -e "${LAST_BACKUP_FILE}" ]; then
		echo "No backup file '${BACKUP_FILE_PREFIX}....tgz' found in ${BACKUP_DIR}. Place a backup file in this location and try again."
		exit 1
	fi
	
	if service_is_running ${ELASTICSEARCH_SERVICE}; then
		echo -e "Elasticsearch is running. Starting procedure to restore '${LAST_BACKUP_FILE}'...\n"
	else
		echo "Elasticsearch is not running. Please start Elasticsearch and try again.."
		exit 1
	fi
	
	export ELASTIC_COMPOSE_DIR="${COMPOSE_DIR}"
	export ELASTIC_SEARCH_BACKUP_DIR="${BACKUP_DIR}" #'backup' dir expected (result of extracting from /var/backup in image on backup)

	export_credentials

	bash ${BASH_OPTS} "${SCRIPT_DIR}/restore.sh" "${BACKUP_FILE_PREFIX}"
	
	if ! service_is_running "${ELASTICSEARCH_SERVICE}"; then
		ek_start
	fi
}

ek_curate() {
	if ! service_is_running; then
		echo "Service is not running, cannot curate. Start Elasticsearch and try again."
		exit 1
	fi
	
	echo "Closing old indices"
	if ! _docker-compose -f curator.yml ${EXTRA_COMPOSE_OPTS} run --rm curator-close-old; then
		echo "Did not close and/or delete any old indices"
	fi
	echo "Purging monitoring indices"
	if ! _docker-compose -f curator.yml ${EXTRA_COMPOSE_OPTS} run --rm curator-purge-monitoring; then
		echo "Did not purge monitoring indices"
	fi
}

_docker-compose() {
	(cd $COMPOSE_DIR && docker-compose $@)
}

service_is_running() {
    if ! (_docker-compose ps $1 |grep -q "Up "); then
        return 1
    else
        return 0
    fi
}

export_credentials() {
	if [ "$SECURITY" -eq 1 ]; then
		eval "$(grep -e "^ELASTICSEARCH_USERNAME" "${COMPOSE_DIR}/.env")"
		eval "$(grep -e "^ELASTICSEARCH_PASSWORD" "${COMPOSE_DIR}/.env")"
		export ELASTICSEARCH_USERNAME ELASTICSEARCH_PASSWORD
	fi
} 

main $@
