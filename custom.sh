#!/usr/bin/env bash

CURATE=0
HELP=0

log() {
    TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')
    #LVL=$1
    MSG=$1
    TAG=$2
    echo "[${TIMESTAMP}] [     ] [${TAG}] ${MSG}"
}

print_usage() {
    echo "  curate      Curates the ElasticSearch indices (close old ones)"
}

main() {
	#
	# Process script arguments
	#
	while [[ $# -gt 0 ]]
	do
	key="$1"
	case $key in
		'curate')
			CURATE=1
			;;

		*)
			echo "Unkown option: $key"
			HELP=1
			;;
	esac
	shift # past argument or value
	done

	#
	# Execute based on mode argument
	#
	if [[ $# -le 0 ]] || [[ ${HELP} -eq 1 ]]; then
		print_usage
	    exit 0
	else
		if [ ${CURATE} -eq 1 ]; then
			(
			cd compose_fluent_kibana &&
			stdbuf -oL ./control.sh curate 2>&1 | while IFS= read -r line
        		do
 		           log "$line" " custom"
		        done
			)
		fi
	fi
}

main $@