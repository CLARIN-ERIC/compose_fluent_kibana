# Elasticsearch/Kibana compose configuration for CLARIN

Compose configuration for (Fluentd) log aggregation using 
[Elasticsearch and Kibana](https://www.elastic.co/products).

Based on the [official Docker images](https://www.docker.elastic.co/), see the following
pages for details:
* [Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)
* [Running Kibana on Docker](https://www.elastic.co/guide/en/kibana/current/docker.html)
 
## Setting up
Out of the box everything should work by just doing
```sh
cd clarin
# create .env file, leaving defaults initially
cp .env-template ../../.env
# start services 
docker-compose up -d
```

This exposes Elasticsearch locally (only!) on port `9200`, which can be used as a sink for fluentd
using the [Elasticsearch Output Plugin](https://docs.fluentd.org/v1.0/articles/out_elasticsearch).

Kibana is available locally over http by connecting to port `5600`. The default account 
(shared between Elasticsearch and Kibana has credentials `elastic`/`changeme`).

## Operation

See the [CLARIN Trac](https://trac.clarin.eu/wiki/SystemAdministration/Monitoring/Kibana)
for information on setup, deployment and configuring access.
