#!/usr/bin/env bash
source "$(dirname $0)/_inc.sh"

SRC_DIR="${ELASTIC_SEARCH_BACKUP_DIR:-/tmp/esbackup/backup}" #matches default in backup script

set -e

function main {
    BACKUP_FILE_PREFIX=$1
    if [ "${BACKUP_FILE_PREFIX}" == "" ]; then
        echo "BACKUP_FILE_PREFIX is required"
        exit 1
    fi

	check_env
	check_service
	set_permissions
	prepare_repo
	prepare_restore
	check_snapshot
	do_restore
	finalize
}

function check_env {
	DOCKER_COMPOSE_OPTS="-f docker-compose.yml -f restore.yml -f proxy.yml"

	if [ ! -d "${SRC_DIR}" ]; then
		echo "Expecting a directory ${SRC_DIR}"
		exit 2
	fi

	if [ -z "$ELASTIC_COMPOSE_DIR" ]; then
		echo "Please set environment variable ELASTIC_COMPOSE_DIR"
		exit 1
	fi

	if [ -z "$ELASTIC_COMPOSE_DIR" ]; then
		echo "Please set environment variable ELASTIC_COMPOSE_DIR"
		exit 1
	fi
	
	AUTH_YML="-f auth.yml"
	if [ -z "$ELASTICSEARCH_USERNAME" ] || [ -z "$ELASTICSEARCH_PASSWORD" ]; then
		echo "User name and password not provided. Will assume not authentication required."
	else
		 DOCKER_COMPOSE_OPTS="${DOCKER_COMPOSE_OPTS} -f auth.yml"
	fi
	
	if [ -z "$SNAPSHOT_NAME" ]; then
		echo "Please set environment variable SNAPSHOT_NAME (see index-0 file for index info)"
		exit 1
	fi

	echo "Restoring from ${SRC_DIR}"
	echo "Using repository/snapshot '${REPO_NAME}/${SNAPSHOT_NAME}'"
}

function prepare_restore {
	echo -e "\nRestarting with backup mounted...\n"	
	export ELASTIC_SEARCH_BACKUP_REPOSITORY_LOCATION="${SRC_DIR}"
	(cd $ELASTIC_COMPOSE_DIR && \
		docker-compose ${EXTRA_COMPOSE_OPTS} ${DOCKER_COMPOSE_OPTS} down && \
		docker-compose ${EXTRA_COMPOSE_OPTS} ${DOCKER_COMPOSE_OPTS} up -d --force-recreate elasticsearch)

	echo "Copying extract script into container"
	(cd $ELASTIC_COMPOSE_DIR && \
		docker cp "$(dirname $0)/internal_extract.sh" "$(docker-compose ps -q elasticsearch | head):/extract.sh")

    echo "Setting permissing on extract script inside container"
    (cd $ELASTIC_COMPOSE_DIR && \
		docker-compose ${EXTRA_COMPOSE_OPTS} exec elasticsearch chmod ugo+x /extract.sh)

    echo "Running extract script inside container"
    (cd $ELASTIC_COMPOSE_DIR && \
		docker-compose ${EXTRA_COMPOSE_OPTS} exec elasticsearch /extract.sh ${BACKUP_FILE_PREFIX})

	echo "Waiting for Elasticsearch..."
	while ! curl -s -f $(curl_credentials_opt) "${ELASTIC_SEARCH_URL}" > /dev/null
	do
		echo "Waiting for Elasticsearch..."
		sleep 5
	done
}

function check_snapshot {
	if curl -s -f $(curl_credentials_opt) "${ELASTIC_SEARCH_URL}/_snapshot/${REPO_NAME}/${SNAPSHOT_NAME}" > /dev/null
	then
		echo "Snapshot found..."
	else
		echo -e "Snapshot to restore not found. Cannot restore. Snapshots info:\n\n"
		echo curl $(curl_credentials_opt) "${ELASTIC_SEARCH_URL}/_snapshot/${REPO_NAME}/*"
		exit 5
	fi
}

function do_restore {
        echo -e "\nCarrying out restore...\n"
        #close all indexes
        curl -s $(curl_credentials_opt) -XPOST "${ELASTIC_SEARCH_URL}/_all/_close" > /dev/null
        #restore everything
        curl -s $(curl_credentials_opt) -XPOST "${ELASTIC_SEARCH_URL}/_snapshot/${REPO_NAME}/${SNAPSHOT_NAME}/_restore?wait_for_completion=true" -H 'Content-Type: application/json' -d'
        {
          "include_global_state": true
        }
        '
	echo -e "\nDone...\n"
}

function finalize {
    echo "Cleaning up"
    (cd $ELASTIC_COMPOSE_DIR && \
		docker-compose ${EXTRA_COMPOSE_OPTS} exec elasticsearch rm -rf /var/backup/* )

	echo "Stopping services"
	(cd $ELASTIC_COMPOSE_DIR && 
		docker-compose ${EXTRA_COMPOSE_OPTS} ${DOCKER_COMPOSE_OPTS} down)
}

main $@