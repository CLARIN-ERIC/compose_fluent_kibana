#!/usr/bin/env bash
source "$(dirname $0)/_inc.sh"

TARGET_DIR="${ELASTIC_SEARCH_BACKUP_DIR:-/tmp/esbackup}"
CONTAINER_BACKUP_DIR="/var/backup"

set -e

function main {
	check_env
	check_service
	cleanup_backup
	set_permissions
	prepare_repo
	do_backup
	extract_backup
	cleanup_backup
}

function check_env {
	if [ -z "$ELASTIC_COMPOSE_DIR" ]; then
		echo "Please set environment variable ELASTIC_COMPOSE_DIR"
		exit 1
	fi

	if [ -z "$ELASTICSEARCH_USERNAME" ] || [ -z "$ELASTICSEARCH_PASSWORD" ]; then
		echo "User name and password not provided. Will assume not authentication required."
	fi

	echo "Backing up to ${TARGET_DIR}"
	echo "Using repository/snapshot '${REPO_NAME}/${SNAPSHOT_NAME}'"	
}



function do_backup {
	echo -e "\nCarrying out backup...\n"
	if ! curl -f $(curl_credentials_opt) -XPUT "${ELASTIC_SEARCH_URL}/_snapshot/${REPO_NAME}/${SNAPSHOT_NAME}?wait_for_completion=true"
	then
		echo "Failed to create backup!"
		cleanup_backup
		exit 5
	fi

	echo -e "\nDone...\n"
}

function extract_backup {
	
	if [ ! -e "${TARGET_DIR}" ]; then
		echo -e "Making target directory ${TARGET_DIR}...\n"
		mkdir -p "${TARGET_DIR}"
	fi

	echo -e "Extracting to ${TARGET_DIR}...\n"
	(cd $ELASTIC_COMPOSE_DIR && \
		docker cp "$(docker-compose ps -q elasticsearch | head):${CONTAINER_BACKUP_DIR}" "${TARGET_DIR}")
}

function cleanup_backup {
	echo -e "Cleaning up...\n"
	(cd $ELASTIC_COMPOSE_DIR && \
		docker-compose ${EXTRA_COMPOSE_OPTS} exec -T elasticsearch bash -c "if [ -d '${CONTAINER_BACKUP_DIR}' ]; then rm -rf ${CONTAINER_BACKUP_DIR}/*; fi")
}

main