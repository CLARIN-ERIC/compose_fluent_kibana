# Common functions and variables for backup.sh and restore.sh

ELASTIC_SEARCH_URL="http://localhost:9200"
CONTAINER_BACKUP_DIR="/var/backup"
ELASTIC_USER_DEFAULT="elastic"
ELASTIC_PASSWORD_DEFAULT="changeme" #actual default elasticsearch password!

REPO_NAME="${ELASTIC_SEARCH_BACKUP_REPO:-clarin_backup}"
SNAPSHOT_NAME="${ELASTIC_SEARCH_BACKUP_SNAPSHOT:-clarin_snapshot}"

check_service() {	
	if ! curl -s -f $(curl_credentials_opt) "${ELASTIC_SEARCH_URL}" > /dev/null
	then
		echo -e "Fatal: could not connect to Elasticsearch! Are the services running and credentials configured correctly?\n\n"
		(cd $ELASTIC_COMPOSE_DIR && docker-compose ps)
		exit 3
	fi
}

curl_credentials_opt() {
	if [ "${ELASTICSEARCH_USERNAME}" ]; then
		echo "-u ${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD}"
	fi
}

prepare_repo() {
	echo -e "Checking backup repository...\n"

	if curl -s -f $(curl_credentials_opt) "${ELASTIC_SEARCH_URL}/_snapshot/${REPO_NAME}" > /dev/null
	then
		echo "Repository '${REPO_NAME}' found..."
	else
		echo "Creating repository..."
		if ! curl $(curl_credentials_opt) -XPUT "${ELASTIC_SEARCH_URL}/_snapshot/${REPO_NAME}" -H 'Content-Type: application/json' -d'
		{
		  "type": "fs",
		  "settings": {
			"location": "/var/backup",
			"compress": true
		  }
		}' 
		then
			echo "Failed to create repository"
			exit 4
		fi
	fi
}

_remove_dir() {
	if [ -n "$1" ] && [ -d "$1" ]; then
		(cd "$1" \
			&& if [ $(ls -f | wc -l) -gt 0 ]; then rm -rf -- *; fi) \
			&& rmdir -- "$1"
	else
		echo "Remove directory: $1 not found"
		return 1
	fi
}

set_permissions() {
	echo -e "Setting target permission...\n"
	(cd $ELASTIC_COMPOSE_DIR && \
		docker-compose exec -T -u root -w / elasticsearch chown -R elasticsearch /var/backup )
}