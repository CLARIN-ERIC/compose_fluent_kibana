#!/usr/bin/env bash

restore() {
    BACKUP_FILE_PREFIX=$1

    echo "Uncompressing backup..."
    INTERNAL_BACKUP_DIR="/var/backup"
    INTERNAL_RESTORE_DIR="/var/restore"

	LAST_BACKUP_FILE=`find "${INTERNAL_RESTORE_DIR}" -maxdepth 1 -name "${BACKUP_FILE_PREFIX}*.tgz" | sort | tail -n 1`
	if [ "${LAST_BACKUP_FILE}" == "" ]; then
		echo "Last backup file not found. Aborting restore"
		exit 1
	fi

	#Ensure INTERNAL_BACKUP_DIR exists and is empty
    if [ -d "${INTERNAL_BACKUP_DIR}" ]; then
            rm -rf ${INTERNAL_BACKUP_DIR}/*
    else
            mkdir -p "${INTERNAL_BACKUP_DIR}"
    fi

    #extract backup file and move in place
	tar zxf "${LAST_BACKUP_FILE}" -C "/tmp"
	mv /tmp/backup/* "/var/backup"
}

restore $@